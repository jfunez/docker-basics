FROM digitallyseamless/nodejs-bower-grunt

RUN mkdir /data/docs

COPY ./docs/reveal.js-3.3.0 /data/docs

WORKDIR /data/docs

RUN npm install

EXPOSE 8001

CMD npm start -- --port 8001
